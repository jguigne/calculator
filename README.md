# calculator

## sous-titre

Git training project.

En **gras**
En _italique_

*1
*2
*3*

 git clone https://gitlab.com/jguigne/calculator.git : import

 git status : permet de voir si un fichier a été modifié

 git config [nom]

 git config [email]

que l'on peut vérifier avec > git config --global --get user.name

ou > git config --global --list

--local par défaut >> .git/config


 git add README.md

 git commit -m "Test de Markdown"

...

tracked > _(edition ou création) / git rm --cached_ > dirty > _add_ > staged > _commit_ > tracked

tracked > _suppression : git rm_

dirty > _suppression : suppr_

...

Pour l'envoi du commit

 git push

...

Dépôts à distance :

 git remote : affiche les serveurs

 git remote -v

 git remote add server2 https://gitlab.com/jguigne/calculator.git

 git push server2


On peut filtrer les types de fichiers dans un .gitignore :

 git add .gitignore


Insérer / supprimer :

 git add --all

 git commit -m "erreur ?"

 git push

...

 git rm --cached erreur.txt

 git commit -m "retropedalage"

 git push

...

  git clone https://gitlab.com/jguigne/calculator.git calculator2

  cd calculator2

  git add main.java

 -> add, commit, push... 
